Date: 09.04.2021
Zeit: 19:00 Uhr - 20:00 Uhr

Teilnehmer: David, Pascal, Daniel, Maria, Julian, Peter

Themen:
1.) Uplink Housing
2.) Klima/Lüftung optimieren


###########################################################

## 1.) Uplink Config:


Die derzeitige Situation stellt sich wie folgt dar:

VKM:
Im VKM ist im Keller ein zentraler Router (sw01vault), der mit Glasfaser an den EdgeSwitch am Dachboden (sw01-roof-vkm) verbunden ist.
Von dort ist eine Antenne 60 GHz (vkm_csph) mit Ethernet angebunden.

sw01vault -- sw01-roof-vkm -- vkm_csph


Conesphere Dach:

3 Antennen die am NanoSwitch hängen:

- csph_vkm

- csph_nig

- csph_krypta (kein Link)


NIG Dach:

- nig_csph Antenne

sw01nig - EdgeSwitch am Dach



Doku + Passwörter:

Den Dauersupport und die Maintainer für den Uplink sind David, Daniel und vchrizz.
Jeder der 3 erhält einen eigenen Useraccouunt. 3 User anlegen für dauersupport (david, damadmai, vchrizz)

Für den User "admin" wird das Standardpasswort geändert und direkt auf den Antenne angebracht. Damit kann zur Not jemand anderer vor Ort zugreifen.
Nachdem man bei physischen Zugriff auf die Antennen sowieso alles reseten und zugreifen könnten, wird das als kein Sicherheitsrisiko angesehen.

Daniel wird diese Passwörter ändern.



### TODO: 

- Standard Passwörter bei den Antennen ändern -> damadmai

- backup config der Antennen und Switche ins git-> damadmai

- werte über mqtt server vorbereiten -> damadmai
  MQTT Statuswerte in env.housing einbauen -> wnagele reden -> damadmai

- diagramm für uplink -> david

- Zugriff vpn-vault -> sw01vault -> sw01-roof-vkm (Port 10) checken und dann VLAN200 raufschalten: david



### Weitere Ziele:

- Alarming in Element Chat als Nachrichten für Uplink (pk as a service), vchrizz fragen, ob er das machen kann
vchrizz: gerne, welche IP soll dabei geprüft werden?

- Antenne am VKM als AP um z.B. im Park ein WLAN mit OLSR zu Testzwecken zur Verfügung hat.

- Verbindung zu WUK für OLSR bei VKM, damit wenn Uplink wegfällt, trotzdem geschaut werden kann, was los ist.

- Kabelverlegung im Dachboden, Kamineinfassung am anderen Ende vom Gebäude notwendig, um Antenne zu montieren - Museum fragen



## 2.) Klima/Lüftung

Schlauch von Gitter verlängern -> Julian/pk ?
2 Test-Varianten:
- Schlauch weiter in der Keller verlängern (Außlass im Keller weiter hinten)
- Schlauch zum Gitterfenster raus

### TODO:

- Lockdown abwarten :)
- Mit dem Museum absprechen -> Mail
- Schlauch erwerben und Montagematerial (Kabelbinder/Draht zum aufhängen)
- Für Variante Gitterfenster, "Anschlusskasten" basteln (zur adaption von Schlauch auf Fenster)

[Schlauch ca. 1€/m, Kabebinder sind evt. schon da, Holz für "Anschlusskasten", über den Daumen gepeilt, 10-15 Euro maximal]


Aktueller (kleiner) Lüfter über Gittertür im Warmgang


